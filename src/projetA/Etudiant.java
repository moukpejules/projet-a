package projetA;

public class Etudiant {
	
	private int id;
	private String nom;
	private String prenom;
	private String identifiant;
	
	 public int getId() {
		    return id;
		}
	     public void setId(int id) {
	        this.id = id;	
	    }
	
	 public String getNom() {
		    return nom;
		}
	     public void setNom(String nom) {
	        this.nom = nom;	
	    }
	     
	 public String getPrenom() {
		    return prenom;
			}
		     public void setPrenom(String prenom) {
		        this.prenom = prenom;	
		    }
		     
  public String getIdentifiant() {
		    return identifiant;
				}
			     public void setIdentifiant(String identifiant) {
			        this.identifiant = identifiant;	
			    }	     
}

