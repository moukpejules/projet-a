package projetA;

import java.awt.EventQueue;
import Bd.EtudiantBd;
import java.sql.*;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import javax.swing.JButton;

public class projetA {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					projetA window = new projetA();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public projetA() {
		initialize();
	//	Connect();
	}
	
	Connection con;
	PreparedStatement pst;
	private JTextField txtnom;
	private JTextField txtprenom;
	private JTextField txtidentifiant;
	private JTextField txtnomr;
	private JTextField txtnome;
	private JTextField txtidentie;
	private JTextField txtuee;
	private JTextField txtnoma;
	private JTextField txtidentia;

/*	public void Connect (){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost/projeta", "root", "");
		}
			catch (ClassNotFoundException ex) {
				
			}
			
			catch (SQLException ex) {
				
			}
	}
		*/
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 650, 386);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "ESPACE ETUDIANT", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 27, 218, 172);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("nom");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel.setBounds(10, 29, 46, 14);
		panel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("prenom");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_1.setBounds(10, 65, 46, 14);
		panel.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("identifiant");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_2.setBounds(10, 104, 46, 14);
		panel.add(lblNewLabel_2);
		
		txtnom = new JTextField();
		txtnom.setBounds(66, 26, 86, 20);
		panel.add(txtnom);
		txtnom.setColumns(10);
		
		txtprenom = new JTextField();
		txtprenom.setBounds(66, 62, 86, 20);
		panel.add(txtprenom);
		txtprenom.setColumns(10);
		
		txtidentifiant = new JTextField();
		txtidentifiant.setBounds(66, 101, 86, 20);
		panel.add(txtidentifiant);
		txtidentifiant.setColumns(10);
		
		JButton btnNewButton = new JButton("connexion");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton.setBounds(10, 138, 89, 23);
		panel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("cancel");
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton_1.setBounds(105, 138, 89, 23);
		panel.add(btnNewButton_1);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "ESPACE ENSEIGNANT", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_1.setBounds(241, 27, 190, 172);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblNewLabel_4 = new JLabel("nom");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_4.setBounds(10, 24, 46, 14);
		panel_1.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("identifiant");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_5.setBounds(10, 60, 46, 14);
		panel_1.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("ue");
		lblNewLabel_6.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_6.setBounds(10, 103, 46, 14);
		panel_1.add(lblNewLabel_6);
		
		txtnome = new JTextField();
		txtnome.setBounds(68, 21, 86, 20);
		panel_1.add(txtnome);
		txtnome.setColumns(10);
		
		txtidentie = new JTextField();
		txtidentie.setBounds(66, 57, 86, 20);
		panel_1.add(txtidentie);
		txtidentie.setColumns(10);
		
		txtuee = new JTextField();
		txtuee.setBounds(66, 100, 86, 20);
		panel_1.add(txtuee);
		txtuee.setColumns(10);
		
		JButton btnNewButton_2 = new JButton("connexion");
		btnNewButton_2.setBounds(10, 138, 89, 23);
		panel_1.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("cancel");
		btnNewButton_3.setBounds(101, 138, 89, 23);
		panel_1.add(btnNewButton_3);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "recherche", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBounds(10, 223, 181, 56);
		frame.getContentPane().add(panel_2);
		panel_2.setLayout(null);
		
		JLabel lblNewLabel_3 = new JLabel("nom");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_3.setBounds(10, 31, 46, 14);
		panel_2.add(lblNewLabel_3);
		
		txtnomr = new JTextField();
		txtnomr.setBounds(45, 28, 86, 20);
		panel_2.add(txtnomr);
		txtnomr.setColumns(10);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(null, "ESPACE ADMIN", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_3.setBounds(441, 27, 183, 172);
		frame.getContentPane().add(panel_3);
		panel_3.setLayout(null);
		
		JLabel lblNewLabel_7 = new JLabel("nom");
		lblNewLabel_7.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_7.setBounds(10, 23, 46, 14);
		panel_3.add(lblNewLabel_7);
		
		JLabel lblNewLabel_8 = new JLabel("identifiant");
		lblNewLabel_8.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_8.setBounds(10, 58, 46, 14);
		panel_3.add(lblNewLabel_8);
		
		txtnoma = new JTextField();
		txtnoma.setBounds(55, 20, 86, 20);
		panel_3.add(txtnoma);
		txtnoma.setColumns(10);
		
		txtidentia = new JTextField();
		txtidentia.setBounds(55, 55, 86, 20);
		panel_3.add(txtidentia);
		txtidentia.setColumns(10);
		
		JButton btnNewButton_4 = new JButton("connexion");
		btnNewButton_4.setBounds(10, 121, 89, 23);
		panel_3.add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("cancel");
		btnNewButton_5.setBounds(109, 121, 89, 23);
		panel_3.add(btnNewButton_5);
	}
}
